package com.harman.lms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.harman.lms.model.BindingDetails;
import com.harman.lms.model.BookDetails;
import com.harman.lms.model.BorrowerDetails;
import com.harman.lms.model.CategoryDetails;
import com.harman.lms.model.ShelfDetails;
import com.harman.lms.model.StaffDetails;
import com.harman.lms.model.StudentDetails;
import com.harman.lms.service.LibraryManagementSystemService;

@RestController
@RequestMapping("/lms")
public class LibraryManagementSystemController {
	@Autowired
	private LibraryManagementSystemService libraryManagementSystemService;
	
	@PostMapping
	@RequestMapping("/addBook")
	public ResponseEntity<BookDetails> addBookDetails(@RequestBody BookDetails bookDetails) {
		ResponseEntity<BookDetails> response = null;
		BookDetails bookDtls = null;
		try {
			bookDtls = libraryManagementSystemService.createBookDetails(bookDetails);
			response = new ResponseEntity<BookDetails>(bookDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<BookDetails>(bookDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/addBinding")
	public ResponseEntity<BindingDetails> addBindingDetails(@RequestBody BindingDetails bindingDetails) {
		ResponseEntity<BindingDetails> response = null;
		BindingDetails bindingDtls = null;
		try {
			bindingDtls = libraryManagementSystemService.createBindingDetails(bindingDetails);
			response = new ResponseEntity<BindingDetails>(bindingDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<BindingDetails>(bindingDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/addCategory")
	public ResponseEntity<CategoryDetails> addCategoryDetails(@RequestBody CategoryDetails categoryDetails) {
		ResponseEntity<CategoryDetails> response = null;
		CategoryDetails categoryDtls = null;
		try {
			categoryDtls = libraryManagementSystemService.createCategoryDetails(categoryDetails);
			response = new ResponseEntity<CategoryDetails>(categoryDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<CategoryDetails>(categoryDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/addShelf")
	public ResponseEntity<ShelfDetails> addShelfDetails(@RequestBody ShelfDetails shelfDetails) {
		ResponseEntity<ShelfDetails> response = null;
		ShelfDetails shelfDtls = null;
		try {
			shelfDtls = libraryManagementSystemService.createShelfDetails(shelfDetails);
			response = new ResponseEntity<ShelfDetails>(shelfDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<ShelfDetails>(shelfDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/addStaff")
	public ResponseEntity<StaffDetails> addShelfDetails(@RequestBody StaffDetails staffDetails) {
		ResponseEntity<StaffDetails> response = null;
		StaffDetails staffDtls = null;
		try {
			staffDtls = libraryManagementSystemService.createStaffDetails(staffDetails);
			response = new ResponseEntity<StaffDetails>(staffDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<StaffDetails>(staffDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/addStudent")
	public ResponseEntity<StudentDetails> addStudentDetails(@RequestBody StudentDetails studentDetails) {
		ResponseEntity<StudentDetails> response = null;
		StudentDetails studentDtls = null;
		try {
			studentDtls = libraryManagementSystemService.createStudentDetails(studentDetails);
			response = new ResponseEntity<StudentDetails>(studentDtls,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<StudentDetails>(studentDtls,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@PostMapping
	@RequestMapping("/borrowBook")
	public ResponseEntity<String> borrowBook(@RequestBody BorrowerDetails borrowerDetails) {
		ResponseEntity<String> response = null;
		String status = null;
		try {
			status = libraryManagementSystemService.borrowBook(borrowerDetails);
			response = new ResponseEntity<String>(status,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			status = e.getMessage();
			response = new ResponseEntity<String>(status,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@PostMapping
	@RequestMapping("/returnBook")
	public ResponseEntity<String> returnBook(@RequestBody BorrowerDetails borrowerDetails) {
		ResponseEntity<String> response = null;
		String status = null;
		try {
			libraryManagementSystemService.returnBook(borrowerDetails);
			status = "Success";
			response = new ResponseEntity<String>(status,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			status= e.getMessage();
			response = new ResponseEntity<String>(status,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@GetMapping
	@RequestMapping("/getBorrowersReachedDueDate")
	public ResponseEntity<List<BorrowerDetails>> getStudentsReachedDueDate() {
		ResponseEntity<List<BorrowerDetails>> response = null;
		List<BorrowerDetails> borrowerList = null;
		try {
			borrowerList = libraryManagementSystemService.getStudentsReachedDueDate();
			response = new ResponseEntity<List<BorrowerDetails>>(borrowerList,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<List<BorrowerDetails>>(borrowerList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@GetMapping
	@RequestMapping("/checkBookAvailabilityByBookTitle")
	public ResponseEntity<String> checkBookAvailabilityByBookTitle(@RequestParam(value="bookTitle") String bookTitle) {
		ResponseEntity<String> response = null;
		String status = null;
		try {
			status = libraryManagementSystemService.checkBookAvailabilityByBookTitle(bookTitle);
			response = new ResponseEntity<String>(status,HttpStatus.OK);
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
	
	@GetMapping
	@RequestMapping("/getBorrowerDetailsByStudentId")
	public ResponseEntity<List<BorrowerDetails>> getBorrowerDetailsByStudentId(@RequestParam(value="studentId") String studentId) {
		ResponseEntity<List<BorrowerDetails>> response = null; 
		List<BorrowerDetails> borrowerList = null;
		try {
			borrowerList = libraryManagementSystemService.getBorrowerDetailsByStudentId(studentId);
			response = new ResponseEntity<List<BorrowerDetails>>(borrowerList,HttpStatus.OK);	
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<List<BorrowerDetails>>(borrowerList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	@GetMapping
	@RequestMapping("/getBookDetailsByCategoryName")
	public ResponseEntity<List<BookDetails>> getBookDetailsByCategoryName(@RequestParam(value="category") String category) {
		ResponseEntity<List<BookDetails>> response = null; 
		List<BookDetails> bookList = null;
		try {
			bookList = libraryManagementSystemService.getBookDetailsByCategoryName(category);
			response = new ResponseEntity<List<BookDetails>>(bookList,HttpStatus.OK);	
		}catch(Exception e) {
			e.printStackTrace();
			response = new ResponseEntity<List<BookDetails>>(bookList,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return response;
	}
}
