package com.harman.lms;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@PropertySource(value="classpath:application.properties")
@EnableTransactionManagement
public class LibraryManagementSystemApplication {

  public static void main(final String[] args) {
    SpringApplication.run(LibraryManagementSystemApplication.class, args);
  }
  
  @Autowired
  Environment environment;

  @Bean(name="entityManagerFactory")
  public LocalSessionFactoryBean sessionFactory() {
      LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
      sessionFactory.setDataSource(dataSource());
      sessionFactory.setPackagesToScan("com.harman.lms.model");
      sessionFactory.setHibernateProperties(hibernateProperties());
      return sessionFactory;
  }

  private Properties hibernateProperties() {
      Properties properties = new Properties();
      properties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
      properties.put("hibernate.show_sql", "true");
      return properties; 
  }

  @Bean
  public DataSource dataSource() {
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName(environment.getRequiredProperty("spring.datasource.driver-class-name"));
      dataSource.setUrl(environment.getRequiredProperty("spring.datasource.url"));
      dataSource.setUsername(environment.getRequiredProperty("spring.datasource.username"));
      dataSource.setPassword(environment.getRequiredProperty("spring.datasource.password"));
      return dataSource;
  }
  
}
