package com.harman.lms.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.harman.lms.model.StudentDetails;

@Repository
public interface StudentDetailsRepository  extends JpaRepository<StudentDetails, Long>{
	
}
