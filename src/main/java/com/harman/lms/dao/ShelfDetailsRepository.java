package com.harman.lms.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.harman.lms.model.ShelfDetails;

@Repository
public interface ShelfDetailsRepository extends JpaRepository<ShelfDetails, Long> {

}
