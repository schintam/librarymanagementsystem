package com.harman.lms.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.harman.lms.model.BookDetails;

@Repository
public interface BookDetailsRepository extends CrudRepository<BookDetails, Long> { 
	
	@Query(value="select No_Copies_Current from book_details b where b.ISBN_Code =:isbnCode", nativeQuery=true)
	int getNoOfCopiesavailable(@Param("isbnCode") int isbnCode);
	
	@Transactional
	@Modifying
	@Query(value="update book_details set No_Copies_Current=:noOfCopiesCurrent where ISBN_Code =:isbnCode", nativeQuery=true)
	void updateNoOfCopiesCurrent(@Param("noOfCopiesCurrent") int noOfCopiesCurrent, @Param("isbnCode") int isbnCode);
	
	@Query(value="select No_Copies_Current from book_details where Book_Title like %:bookTitle%", nativeQuery=true)
	List<Integer> getNoOfCopiesavailableByBookTitle(@Param("bookTitle") String bookTitle);
	
	@Query(value="select * from book_details b, category_details c where c.Category_Name=:category and b.Category_id=c.Category_Id", nativeQuery=true)
	List<BookDetails> getBookDetailsByCategoryName(@Param("category") String category);
}
