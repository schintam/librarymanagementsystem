package com.harman.lms.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.harman.lms.model.BindingDetails;

@Repository
public interface BindingDetailsRepository extends CrudRepository<BindingDetails, Long> {

}
