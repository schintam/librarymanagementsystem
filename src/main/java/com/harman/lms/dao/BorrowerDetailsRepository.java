package com.harman.lms.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.harman.lms.model.BorrowerDetails;

public interface BorrowerDetailsRepository  extends CrudRepository<BorrowerDetails, Long>{
	
	@Transactional
	@Modifying
	@Query(value="update borrower_details set Actual_Return_Date=:actulaReturnDate where Book_Id =:bookId and Borrower_Id=:borrowerId and Actual_Return_Date is null", nativeQuery=true)
	void updateAcutalReturnDate(@Param("actulaReturnDate") LocalDate actulaReturnDate, @Param("bookId") int bookId, @Param("borrowerId") int borrowerId);
	
	@Query(value="select id from borrower_details b where b.Borrower_Id =:borrowerId and Book_Id=:bookId and Actual_Return_Date is null", nativeQuery=true)
	Integer isBookBorrowed(@Param("borrowerId") int borrowerId, @Param("bookId") int bookId);
	
	@Query(value="select * from student_details s, borrower_details b where b.Borrower_Id =s.Borrower_Id and b.Actual_Return_Date is null and Borrowed_TO<CURDATE()", nativeQuery=true)
	List<BorrowerDetails> getBorrowersReachedDueDate();
	
	@Query(value="select * from student_details s, borrower_details b where b.Borrower_Id =s.Borrower_Id and b.Actual_Return_Date is null and s.Student_Id=:studentId", nativeQuery=true)
	List<BorrowerDetails> getBorrowerDetailsByStudentId(@Param("studentId") String studentId);
}
