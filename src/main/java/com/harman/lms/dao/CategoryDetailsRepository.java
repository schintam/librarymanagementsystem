package com.harman.lms.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.harman.lms.model.CategoryDetails;

@Repository
public interface CategoryDetailsRepository extends CrudRepository<CategoryDetails, Long> {

}
