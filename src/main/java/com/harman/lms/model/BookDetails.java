package com.harman.lms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="book_details")
public class BookDetails implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ISBN_Code")
	private int isbnCode;
	@Column(name="Book_Title")
	private String bookTitle;
	@Column(name="Language")
	private String language;
	@Column(name="Binding_Id")
	private int bindingId;
	@Column(name="No_Copies_Actual")
	private int noOfCopiesActula;
	@Column(name="No_Copies_Current")
	private int noOfCopiesCurrent;
	@Column(name="Category_id")
	private int categoryId;
	@Column(name="Publication_year")
	private int publicationYear;
	@Column(name="Shelf_Id")
	private int shelfId;
	
	public int getIsbnCode() {
		return isbnCode;
	}
	public void setIsbnCode(int isbnCode) {
		this.isbnCode = isbnCode;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public int getBindingId() {
		return bindingId;
	}
	public void setBindingId(int bindingId) {
		this.bindingId = bindingId;
	}
	public int getNoOfCopiesActula() {
		return noOfCopiesActula;
	}
	public void setNoOfCopiesActula(int noOfCopiesActula) {
		this.noOfCopiesActula = noOfCopiesActula;
	}
	public int getNoOfCopiesCurrent() {
		return noOfCopiesCurrent;
	}
	public void setNoOfCopiesCurrent(int noOfCopiesCurrent) {
		this.noOfCopiesCurrent = noOfCopiesCurrent;
	}
	public int getPublicationYear() {
		return publicationYear;
	}
	public void setPublicationYear(int publicationYear) {
		this.publicationYear = publicationYear;
	}
	public int getShelfId() {
		return shelfId;
	}
	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	@Override
	public String toString() {
		return "BookDetails [isbnCode=" + isbnCode + ", bookTitle=" + bookTitle + ", language=" + language
				+ ", bindingId=" + bindingId + ", noOfCopiesActula=" + noOfCopiesActula + ", noOfCopiesCurrent="
				+ noOfCopiesCurrent + ", categoryId=" + categoryId + ", publicationYear=" + publicationYear
				+ ", shelfId=" + shelfId + "]";
	}
	
}
