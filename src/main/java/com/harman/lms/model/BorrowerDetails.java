package com.harman.lms.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="borrower_details")
public class BorrowerDetails  implements Serializable{
	@Column(name="id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(name="Borrower_Id")
	private int borrowerId;
	@Column(name="Book_Id")
	private int bookId;
	@Column(name="Borrowed_From")
	private LocalDate borrowedFrom;
	@Column(name="Borrowed_TO")
	private LocalDate borrowedTo;
	@Column(name="Actual_Return_Date")
	private LocalDate actulaReturnDate;
	@Column(name="Issued_by")
	private int issuedBy;
	
	public int getBorrowerId() {
		return borrowerId;
	}
	public void setBorrowerId(int borrowerId) {
		this.borrowerId = borrowerId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public LocalDate getBorrowedFrom() {
		return borrowedFrom;
	}
	public void setBorrowedFrom(LocalDate borrowedFrom) {
		this.borrowedFrom = borrowedFrom;
	}
	public LocalDate getBorrowedTo() {
		return borrowedTo;
	}
	public void setBorrowedTo(LocalDate borrowedTo) {
		this.borrowedTo = borrowedTo;
	}
	public LocalDate getActulaReturnDate() {
		return actulaReturnDate;
	}
	public void setActulaReturnDate(LocalDate actulaReturnDate) {
		this.actulaReturnDate = actulaReturnDate;
	}
	public int getIssuedBy() {
		return issuedBy;
	}
	public void setIssuedBy(int issuedBy) {
		this.issuedBy = issuedBy;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}		
}
