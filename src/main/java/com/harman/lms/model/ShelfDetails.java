package com.harman.lms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="shelf_details")
public class ShelfDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Shelf_id")
	private int shelfId;
	@Column(name="Shelf_No")
	private int shelfNumber;
	@Column(name="Floor_No")
	private int floorNumber;
	
	public int getShelfId() {
		return shelfId;
	}
	public void setShelfId(int shelfId) {
		this.shelfId = shelfId;
	}
	public int getShelfNumber() {
		return shelfNumber;
	}
	public void setShelfNumber(int shelfNumber) {
		this.shelfNumber = shelfNumber;
	}
	public int getFloorNumber() {
		return floorNumber;
	}
	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}
	@Override
	public String toString() {
		return "ShelfDetails [shelfId=" + shelfId + ", shelfNumber=" + shelfNumber + ", floorNumber=" + floorNumber
				+ "]";
	}	
}
