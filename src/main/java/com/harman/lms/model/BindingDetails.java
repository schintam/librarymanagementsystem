package com.harman.lms.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="binding_details")
public class BindingDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Binding_id")
	private int bindingId;
	@Column(name="Binding_Name")
	private String bindingName;
	
	public int getBindingId() {
		return bindingId;
	}
	public void setBindingId(int bindingId) {
		this.bindingId = bindingId;
	}
	public String getBindingName() {
		return bindingName;
	}
	public void setBindingName(String bindingName) {
		this.bindingName = bindingName;
	}
	@Override
	public String toString() {
		return "BindingDetails [bindingId=" + bindingId + ", bindingName=" + bindingName + "]";
	}	
}
