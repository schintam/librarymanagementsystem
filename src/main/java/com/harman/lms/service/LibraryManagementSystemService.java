package com.harman.lms.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.harman.lms.dao.BindingDetailsRepository;
import com.harman.lms.dao.BookDetailsRepository;
import com.harman.lms.dao.BorrowerDetailsRepository;
import com.harman.lms.dao.CategoryDetailsRepository;
import com.harman.lms.dao.ShelfDetailsRepository;
import com.harman.lms.dao.StaffDetailsRepository;
import com.harman.lms.dao.StudentDetailsRepository;
import com.harman.lms.model.BindingDetails;
import com.harman.lms.model.BookDetails;
import com.harman.lms.model.BorrowerDetails;
import com.harman.lms.model.CategoryDetails;
import com.harman.lms.model.ShelfDetails;
import com.harman.lms.model.StaffDetails;
import com.harman.lms.model.StudentDetails;

@Service
public class LibraryManagementSystemService {
	@Autowired
	private BookDetailsRepository bookDetailsRepository;
	
	@Autowired
	private BindingDetailsRepository bindingDetailsRepository;
	
	@Autowired
	private CategoryDetailsRepository categoryDetailsRepository;
	
	@Autowired
	private ShelfDetailsRepository shelfDetailsRepository; 
	
	@Autowired
	private StaffDetailsRepository staffDetailsRepository;
	
	@Autowired
	private StudentDetailsRepository studentDetailsRepository;
	
	@Autowired
	private BorrowerDetailsRepository borrowerDetailsRepository;
	
	public BookDetails createBookDetails(BookDetails bookDetails) {
		return bookDetailsRepository.save(bookDetails);
	}
	
	public BindingDetails createBindingDetails(BindingDetails bindingDetails) {
		return bindingDetailsRepository.save(bindingDetails);
	}
	
	public CategoryDetails createCategoryDetails(CategoryDetails categoryDetails) {
		return categoryDetailsRepository.save(categoryDetails);
	}
	
	public ShelfDetails createShelfDetails(ShelfDetails shelfDetails) {
		return shelfDetailsRepository.save(shelfDetails);
	}
	
	public StaffDetails createStaffDetails(StaffDetails staffDetails) {
		return staffDetailsRepository.save(staffDetails);
	}
	
	public StudentDetails createStudentDetails(StudentDetails studentDetails) {
		return studentDetailsRepository.save(studentDetails);
	}
	public String borrowBook(BorrowerDetails borrowerDetails) {
		Integer id = borrowerDetailsRepository.isBookBorrowed(borrowerDetails.getBorrowerId(), borrowerDetails.getBookId());
		if(id!=null && id>0) {
			return "Book already Borrowed";
		}
		int noOfcopies = bookDetailsRepository.getNoOfCopiesavailable(borrowerDetails.getBookId());
		if(noOfcopies>0) {
			System.out.println("Book available");
			borrowerDetailsRepository.save(borrowerDetails);
			bookDetailsRepository.updateNoOfCopiesCurrent(noOfcopies-1,borrowerDetails.getBookId());			
			return "Success";
		}else {
			System.out.println("Book not available");
			return "Book Not available";
		}
		
	}
	public void returnBook(BorrowerDetails borrowerDetails) {
		int noOfcopies = bookDetailsRepository.getNoOfCopiesavailable(borrowerDetails.getBookId());
		borrowerDetailsRepository.updateAcutalReturnDate(LocalDate.now(), borrowerDetails.getBookId(),borrowerDetails.getBorrowerId());
		bookDetailsRepository.updateNoOfCopiesCurrent(noOfcopies+1,borrowerDetails.getBookId());
	}
	
	public List<BorrowerDetails> getStudentsReachedDueDate(){
		return borrowerDetailsRepository.getBorrowersReachedDueDate();
	}
	
	public String checkBookAvailabilityByBookTitle(String bookTitle) {
		List<Integer> copiesList= bookDetailsRepository.getNoOfCopiesavailableByBookTitle(bookTitle);
		int numberOfCopies = 0;
		for(Integer copies : copiesList) {
			numberOfCopies = numberOfCopies + copies;
		}
		if(numberOfCopies > 0) {
			return "Available "+ numberOfCopies +" copies.";
		}else {
			return "Not Available";
		}
	}
	
	public List<BorrowerDetails> getBorrowerDetailsByStudentId(String studentId){
		return borrowerDetailsRepository.getBorrowerDetailsByStudentId(studentId);
	}
	
	public List<BookDetails> getBookDetailsByCategoryName(String category){
		return bookDetailsRepository.getBookDetailsByCategoryName(category);
	}
}
